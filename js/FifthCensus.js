//柱状图1

(function(){
    var myChart = echarts.init(document.querySelector('.bar .chart'));
    var years = ['1991', '1992', '1993', '1994', '1995', '1996', '1997', '1998', '1999','2000'];
    var jdData = [
        ['(亿)'],
        ['(亿)'],
        ['(亿)'],
        ['(亿)'],
        ['(亿)'],
        ['(亿)'],
        ['(亿)'],
        ['(亿)'],
        ['(亿)'],
        ['(亿)'],
    ]
    var data = [
        [11.51],
        [11.65],
        [11.78],
        [11.92],
        [12.05],
        [12.18],
        [12.3],
        [12.42],
        [12.53],
        [12.63],

    ];
    option = {

        baseOption: {
            // backgroundColor: '#2c343c', //背景颜色
            timeline: {
                data: years,
                axisType: 'category',
                autoPlay: true,
                playInterval: 800, //播放速度

                left: '5%',
                right: '5%',
                bottom: '0%',
                // width: '90%',
                //  height: null,
                label: {
                    normal: {
                        textStyle: {

                            color: 'red',
                        }
                    },
                    emphasis: {
                        textStyle: {
                            color: 'red'
                        }
                    }
                },
                symbolSize: 10,
                lineStyle: {
                    color: '#red'
                },
                checkpointStyle: {
                    borderColor: '#red',
                    borderWidth: 2
                },
                controlStyle: {
                    showNextBtn: true,
                    showPrevBtn: true,
                    normal: {
                        color: '#ff8800',
                        borderColor: '#ff8800'
                    },
                    emphasis: {
                        color: 'red',
                        borderColor: 'red'
                    }
                },

            },
            title: {
                text: '',
                right: '2%',
                bottom: '25%',
                textStyle: {
                    fontSize: 30,
                    color: 'yellow' //标题字体颜色
                }
            },
            tooltip: {
                'trigger': 'axis'
            },
            calculable: true,
            grid: {
                left: '8%',
                right: '5%',
                bottom: '20%',
                top: '0%',
                containLabel: true
            },
            label: {
                normal: {
                    textStyle: {
                        color: 'red'
                    }
                }
            },
            yAxis: [{

                nameGap: 50,
                offset: '37',
                'type': 'category',
                interval: 50,
                //inverse: ture,//图表倒叙或者正序排版
                data: '',
                nameTextStyle: {
                    color: 'red'
                },


                axisLabel: {
                    //rotate:45,
                    show: false,
                    textStyle: {
                        fontSize: 32,

                        // color: function(params, Index) { // 标签国家字体颜色

                            //color:function(d){return "#"+Math.floor(Math.random()*(256*256*256-1)).toString(16);//随机生成颜色

                            // var colorarrays = [//'#6bc0fb'
                            // ,'#7fec9d', '#fedd8b', '#ffa597', '#84e4dd', '#749f83',
                                // '#ca8622'//, '#bda29a', '#a8d8ea', '#aa96da', '#fcbad3', '#ffffd2',
                            //     '#f38181', '#fce38a', '#eaffd0', '#95e1d3', '#e3fdfd', '#749f83', '#ca8622'
                            // ];

                            //console.log("111", Index, colorarrays[Index],params); //打印序列

                            // return colorarrays[jdData[0].indexOf(params)];
                        // },
                        color:"red",

                    } //rotate:45,
                    // interval: 50
                },
                axisLine: {

                    lineStyle: {
                        color:'balck' //Y轴颜色
                    },
                },
                splitLine: {
                    show: false,
                    lineStyle: {
                        color: 'balck'
                    }
                },

            }],
            xAxis: [{
                'type': 'value',
                'name': '',

                splitNumber: 8, //轴线个数
                nameTextStyle: {
                    color: 'balck'
                },
                axisLine: {
                    lineStyle: {
                        color: '#ffa597' //X轴颜色
                    }
                },
                axisLabel: {
                    formatter: '{value} '
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        color:"rgba(255,255,255,0.1)"
                    }
                },
            }],
            series: [{
                    'name': '',
                    'type': 'bar',
                    markLine: {
                        label: {
                            normal: {
                                show: false
                            }
                        },
                        lineStyle: {
                            normal: {
                                color: 'red',
                                width: 3
                            }
                        },
                    },
                    label: {
                        normal: {
                            show: true,
                            position: 'right', //数值显示在右侧
                            formatter: '{c}'
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: function(params) {
                                // build a color map as your need.
                                //color:function(d){return "#"+Math.floor(Math.random()*(256*256*256-1)).toString(16);//随机生成颜色         
                                var colorList = ['#6bc0fb', '#7fec9d', '#fedd8b', '#ffa597', '#84e4dd', '#749f83',
                                    '#ca8622', '#bda29a', '#a8d8ea', '#aa96da', '#fcbad3', '#ffffd2',
                                    '#f38181', '#fce38a', '#eaffd0', '#95e1d3', '#e3fdfd', '#749f83', '#ca8622'
                                ];
                                // return colorList[params.dataIndex]

                                console.log("111", params.name); //打印序列
                                return colorList[jdData[0].indexOf(params.name)];
                            },

                        }
                    },
                },

                {
                    'name': '',
                    'type': 'bar',
                    markLine: {
                        
                        
                        label: {
                            normal: {
                                show: false
                            }
                        },
                        lineStyle: {
                            
                            normal: {
                                color: 'red',
                                width: 3
                            }
                        },
                    },
                    barWidth: '50%',
                    barGap: '-100%',
                    label: {
                        normal: {
                            show: true,
                            fontSize: 16,  //标签国家字体大小
                            position: 'left', //数值显示在右侧
                            formatter: function(p) {
                                return p.name;
                            }
                        }
                    },
                    itemStyle: {
                        normal: {
                            
                            
                            color: function(params) {
                                // build a color map as your need.
                                //color:function(d){return "#"+Math.floor(Math.random()*(256*256*256-1)).toString(16);//随机生成颜色         
                                var colorList = ['#6bc0fb', '#7fec9d', '#fedd8b', '#ffa597', '#84e4dd', '#749f83',
                                    '#ca8622', '#bda29a', '#a8d8ea', '#aa96da', '#fcbad3', '#ffffd2',
                                    '#f38181', '#fce38a', '#eaffd0', '#95e1d3', '#e3fdfd', '#749f83', '#ca8622'
                                ];
                                // return colorList[params.dataIndex]

                                // console.log("111", params.name); //打印序列
                                return colorList[jdData[0].indexOf(params.name)];
                            },

                        }
                    },
                }
            ],

            animationEasingUpdate: 'quinticInOut',
            animationDurationUpdate: 1500, //动画效果
        },

        options: []
    };
    for (var n = 0; n < years.length; n++) {

        var res = [];
        //alert(jdData.length);
        for (j = 0; j < data[n].length; j++) {
            res.push({
                name: jdData[n][j],
                value: data[n][j]
            });

        }

        res.sort(function(a, b) {
            return b.value - a.value;
        }).slice(0, 6);

        res.sort(function(a, b) {
            return a.value - b.value;
        });


        var res1 = [];
        var res2 = [];
        //console.log(res);
        for (t = 0; t < res.length; t++) {
            res1[t] = res[t].name;
            res2[t] = res[t].value;
        }
        option.options.push({
            title: {
                text: years[n] + '年'
            },
            yAxis: {
                data: res1,
            },
            series: [{
                data: res2
            }, {
                data: res2
            }]
        });
        }
        myChart.setOption(option);
        //让图表跟随屏幕自动的去适应
        window.addEventListener('resize', function() {
            myChart.resize();
        });
    })();


    //柱状图2
    (function() {
        //实例化对象
        var myChart=echarts.init(document.querySelector('.bar2 .chart'));
        //颜色声明
        var myColor = ["purple","blue","#1089E7","#F57474","#56D0E3","#F8B448","#8B78F6","#1CBB70"];
        //指定配置和数据
        var option = {
        
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                type: 'shadow'
                }
            },
        
            grid: {
                top:"10px",
                left:"15%",
                right:"15%",
                bottom:"10%",
                // containLabel:"true"
            },
            // 不显示x轴相关信息
            xAxis: {
                show:false
            },
            yAxis: [
                {
                type: 'category',
                inverse:true,
                data: ['未上过学', '小  学', '初  中', '高  中', '大学专科','大学本科','研究生'],
                //不显示y轴的线
                axisLine:{
                    show:false
                },
                //不显示刻度
                axisTick: {
                    show:false 
                },
                //把刻度标签的文字改为白色
                axisLabel: {
                    color:"#fff"
                }
            },
                {
                    data:["6-14岁","15-24岁","25-34岁","35-44岁","45-54岁","55-64岁","65岁以上"],
                    inverse:true,
                    //不显示y轴信息
                    axisLine:{
                        show:false
                    },
                    //不显示刻度
                    axisTick:{
                        show:false
                    },
                    //把刻度标签的文字设置为白色
                    axisLabel:{
                        color:"#fff"
                    }
                }
            ],
            series: [
                {
                    name: '人数',
                    type: 'bar',
                    yAxisIndex:0,
                    data: [131057874, 227301632, 198152055, 242779923, 184347724,139979756,118927158],
                    //修改第一组柱子的圆角
                    itemStyle: {
                            barBorderRadius: 20,
                            color:function(params){
                                return myColor[params.dataIndex]
                            }
                    },
                    //柱子之间的距离
                    barCategoryGap: 50,
                    //柱子的宽度
                    barWidth: 10,
                    //显示柱子内的文字
                    label:{
                        show:true,
                        position:"inside",
                        //{c}会自动解析data里边的数据
                        formcatter:"{c}%"
                    }
                },
                {
                    name: '基数',
                    type: 'bar',
                    yAxisIndex:1,
                    barCategoryGap:50,
                    barWidth:15,
                    data: [300000000, 300000000, 300000000, 
                        300000000, 300000000,300000000,300000000],
                    itemStyle:{
                        color:"none",
                        borderColor:"#00c1de",
                        borderWidth:3,
                        barBorderRadius:15
                    },
                }
            ]
        };
        //把配置给实例对象
        myChart.setOption(option);
        //让图表跟随屏幕自动的去适应
        window.addEventListener('resize', function() {
        myChart.resize();
        });
    })();

    // 折线图1
    (function() {
        //实例化对象
        var myChart = echarts.init(document.querySelector(".line .chart"));
        //指定配置
        var option = {
            color: ["#00f2f1","#ed3f35","yellow"],//修改线的颜色
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['14岁及以下', '15至64岁', '65岁及以上'],
                textStyle:{
                    color:"#4c9bfd"
                },
                right:"10%"
            },
            grid: {
                top:"20%",
                left:"3%",
                right: '4%',
                bottom: '3%',
                show:true,//显示边框
                borderColor:"#012f4a",//边框颜色
                containLabel: true//包含文字刻度在内
            },
            
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: ['1991','1992','1993','1994','1995','1996','1997','1998','1999','2000'],
                axisTick: {
                    show:false//去除刻度线
                },
                axisLabel: {
                    color:"#4c9bfd"//文本颜色
                },
                axisLine:{
                    show:false//去除轴线
                }
            },
            yAxis: {
                type: 'value',
                axisTick: {
                    show:false//去除刻度线
                },
                axisLabel: {
                    color:"#4c9bfd"//文本颜色
                },
                axisLine:{
                    show:false//去除轴线
                },
                splitLine:{
                    lineStyle:{
                        color:"#012f4a"//y轴分割线的颜色
                    }
                }
            },
            series: [
                {
                    name: '14岁及以下',
                    type: 'line',
                    data: [324546177, 328596758, 326580984, 325280651, 325698399, 320435561, 
                        319849182,320906193,319203340,312993544],
                    smooth:true
                },
                {
                    name: '15至64岁',
                    type: 'line',
                    data: [756677632, 768952440, 782325624, 794860325, 805367614, 820493214, 
                        831124788,839669246,849907147,863641938],
                    smooth:true
                },
                {
                    name: '65岁及以上',
                    type: 'line',
                    data: [65520712, 67420802, 69533392, 71694024, 73788987, 76621226, 
                        79101030,81359560,83624513,86009518],
                    smooth:true
                }
        ]};
        //将配置给实例化对象
        myChart.setOption(option);
        //让图表跟随屏幕自动的去适应
        window.addEventListener("resize",function() {
            myChart.resize();
        });

    })();

    //折线图2
    (function(){
        var myChart = echarts.init(document.querySelector('.line2 .chart'));
        var option = {
    
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    label: {
                        backgroundColor: '#6a7985'
                    }
                }
            },
            legend: {
                top:"0%",
                textStyle:{
                    color:"rgba(255,255,255,0,5)",
                    fontSize:"12"
                }
            },
        
            grid: {
                left:"10",
                top:"30",
                right: '10',
                bottom: '10',
                containLabel: true
            },
            xAxis: [
                {
                    type: 'category',
                    boundaryGap: false,
                    data: ['1991', '1992', '1993', '1994', '1995', '1996', '1997',
                    '1998','1999','2000'],
                    axisLabel:{
                        textStyle:{
                            color:"rgba(255,255,255,0.6)",
                            fontSize:12
                        }
                    },
                    //x轴线的颜色
                    axisLine:{
                        lineStyle:{
                            color:"rgba(255,255,255,0.2)"
                        }
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    axisTick:{
                        show:false
                    },
                    axisLine:{
                        lineStyle:{
                            color:"rgba(255,255,255,0.1)"
                        }
                    },
                    axisLabel:{
                        textStyle:{
                            color:"rgba(255,255,255,0.6)",
                            fontSize:12
                        }
                    },
                    splitLine:{
                        lineStyle:{
                            color:"rgba(255,255,255,0.1)"
                        }
                    }
                }
            ],
            series: [
                {
                    name: '女',
                    type: 'line',
                    areaStyle: {
                        //渐变颜色
                        color: new echarts.graphic.LinearGradient(
                            0,
                            0,
                            0,
                            1,
                            [
                                {
                                    offset:0,
                                    color:"rgba(255,255,255,0.4)"//渐变色的起始颜色
                                },
                                {
                                    offset:0.8,
                                    color:"rgba(255,255,255,0.1)"//渐变色的结束颜色
                                }
                            ],
                            false
                        ),
                        shadowColor:"rgba(0,0,0,0.1)"
                    },
                    symbol:"circle",
                    symbolSize:5,
                    //设置拐点边框和颜色
                    itemStyle:{
                        color:"#0184d5",
                        borderColor:"rgba(221,220,107,0.1)",
                        borderWidth:12
                    },
                    showSymbol:false,//开始不显示，鼠标经过才显示
                    data: [560312586, 567224558, 573790071, 580322015, 586672507, 
                        592868311, 598982994,604764109,610004104,614775825],
                    smooth:true,
                    lineStyle:{
                        color:"#0184d5"
                    }
                },
                
                {
                    name: '男',
                    type: 'line',
                    areaStyle: {
                        //渐变颜色
                        color: new echarts.graphic.LinearGradient(
                            0,
                            0,
                            0,
                            1,
                            [
                                {
                                    offset:0,
                                    color:"rgba(0,216,135,0.4)"//渐变色的起始颜色
                                },
                                {
                                    offset:0.8,
                                    color:"rgba(0,216,135,0.1)"//渐变色的结束颜色
                                }
                            ],
                            false
                        ),
                        shadowColor:"rgba(0,0,0,0.1)"
                    },
                    symbol:"circle",
                    symbolSize:5,
                    //设置拐点边框和颜色
                    itemStyle:{
                        color:"#00d887",
                        borderColor:"rgba(221,220,107,0.1)",
                        borderWidth:12
                    },
                    showSymbol:false,
                    emphasis: {
                        focus: 'series'
                    },
                    data: [590467414, 597745442, 604649929, 611512985, 618182493, 
                        624681689, 631092006,637170891,642730896,647869175],
                    smooth:true
                }
            ]
        };
        myChart.setOption(option);
    })();

//饼状图1
(function(){
    var myChart = echarts.init(document.querySelector(".pie .chart"));
    var option = {
        color:["#065aab","#066eab","#0682ab"
        // "#06a0ab","#0696ab"
    ],
        tooltip: {
            trigger: 'item'
        },
        legend: {
            bottom:0,
            //设置小图标的大小
            itemWidth:10,
            itemHeight:10,
            //修改图例组件
            textStyle:{
                color:"rgba(255,255,255,0.5)",
                fontSize:"12"
            }
        },
        series: [
            {
                name: '年龄状态',
                type: 'pie',
                //redius修改饼形图的大小
                radius: ['40%', '60%'],
                center:["50%","45%"],
                avoidLabelOverlap: false,
                label: {
                    show: true,
                    // position: 'center'
                },
                emphasis: {
                    label: {
                        show: true,
                        fontSize: '30',
                        fontWeight: 'bold'
                    }
                },
                //链接图形和对应文字的线是否显示
                labelLine: {
                    show: false
                },
                data: [
                    {value: 221322621, name: '0-14岁'},
                    {value: 933893808, name: '15-59岁'},
                    {value: 177594440, name: '60岁及以上'},
                    // {value: 484, name: '20-40岁'},
                    // {value: 300, name: '40-100岁'}
                ]
            }
        ]
    };
    //将指定项配置给实例化对象
    myChart.setOption(option);
    //让图表跟随屏幕自动的去适应
    window.addEventListener("resize",function() {
        myChart.resize();
    });
})();

//饼形图2
(function(){
    var myChart = echarts.init(document.querySelector(".pie2 .chart"));
    var option = {
        color:[
            "#006cff",
            "#60cda0",
            "#ed8884",
            "#ff9f7f",
            "#0096ff",
            "#9fe6b8",
            "#32c5e9",
            "#1d9dff"],
        tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b} : {c} ({d}%)'
        },
        legend: {
            bottom:"0",
            itemWidth: 10,
            itemHeight: 10,
            textStyle:{
                color:"rgba(255,255,255,0.5)",
                fontSize:"10"
            }
            // data: ['rose1', 'rose2', 'rose3', 'rose4', 'rose5', 'rose6', 'rose7', 'rose8']
        },
       
        series: [
            
            {
                name: '人口分布',
                type: 'pie',
                radius: [10, 70],
                center: ['50%', '50%'],
                roseType: 'radius',
                //图像的文字标签
                label:{
                    fontSize:10
                },
                //链接图形和文字的线条
                labelLine:{
                    length:6,
                    length2:8
                },
                data: [
                    {value: 91236854, name: '天津市'},
                    {value: 89971789, name: '河南省'},
                    {value: 85225007, name: '山东省'},
                    {value: 82348296, name: '广东省'},
                    {value: 73043577, name: '四川省'},
                    {value: 66684419, name: '海南省'},
                    {value: 63274173, name: '江苏省'},
                    {value: 59508870, name: '河北省'}
                ]
            }
        ]
    };
    
    myChart.setOption(option);
    //让图表跟随屏幕自动的去适应
    window.addEventListener("resize",function() {
        myChart.resize();
    });
})();



//地图模块
(function() {
    var myChart = echarts.init(document.querySelector(".map .chart"));
    var name_title = "以省为单位的人口数据可视化";
    // var subname = '虚拟数据(随便写的)'
    var nameColor = '#4444FF';
    var name_fontFamily = '等线';
    var name_fontSize = 25;
    // var subname_fontSize = 15;
    var mapName = 'china';
    // 随便编的数据
    var data = [{
            'name': '北京',
            'value': 13569194
        },
        {
            'name': '天津',
            'value': 9848731
        },
        {
            'name': '河北',
            'value': 66684419
        },
        {
            'name': '山西',
            'value': 32471242
        },
        {
            'name': '内蒙古',
            'value': 23323347
        },
        {
            'name': '辽宁',
            'value': 41824412
        },
        {
            'name': '吉林',
            'value': 26802191
        },
        {
            'name': '黑龙江',
            'value': 36237576
        },
        {
            'name': '上海',
            'value': 16407734
        },
        {
            'name': '江苏',
            'value': 73043577
        },
        {
            'name': '浙江',
            'value': 45930651
        },
        {
            'name': '安徽',
            'value': 58999948
        },
        {
            'name': '福建',
            'value': 34097947
        },
        {
            'name': '江西',
            'value': 40397598
        },
        {
            'name': '山东',
            'value': 89971789
        },
        {
            'name': '河南',
            'value': 91236854

        },
        {
            'name': '湖北',
            'value': 59508870
        },
        {
            'name': '湖南',
            'value': 63274173
        },
        {
            'name': '广东',
            'value': 85225007
        },
        {
            'name': '广西',
            'value': 43854538
        },
        {
            'name': '海南',
            'value': 7559035
        },
        {
            'name': '重庆',
            'value': 30512763
        },
        {
            'name': '四川',
            'value': 82348296
        },
        {
            'name': '贵州',
            'value': 35247695
        },
        {
            'name': '云南',
            'value': 42360089
        },
        {
            'name': '西藏',
            'value': 2616329
        },
        {
            'name': '陕西',
            'value': 35365072
        },
        {
            'name': '甘肃',
            'value': 25124282
        },
        {
            'name': '青海',
            'value': 4822963
        },
        {
            'name': '宁夏',
            'value': 5486393
        },
        {
            'name': '新疆',
            'value': 18459511
        },
        {
            'name': '香港',
            'value': 0
        },
        {
            'name': '澳门',
            'value': 0
        },
        {
            'name': '台湾',
            'value': 0
        }
    ];
    // var dom1 = document.getElementById("myChart");
    // var myChart = echarts.init(dom1);
    /*获取地图数据*/
    myChart.showLoading();
    var mapFeatures = echarts.getMap(mapName).geoJson.features;
    myChart.hideLoading();
    var geoCoordMap = {};
    mapFeatures.forEach(function(v) {
        // 地区名称
        var name = v.properties.name;
        // 地区经纬度
        geoCoordMap[name] = v.properties.cp;

    });
    // 用于处理effectScatter气泡散点图，气泡的大小
    var max = 4000,
        min = 10;
    var maxSize4Pin = 100,
        minSize4Pin = 20;


    // 柱状图数据处理
    var yData = [];
    var barData = data.filter(function(item) {
        return item.value > 0;
    });
    barData = barData.sort(function(a, b) {
        return b.value - a.value;
    });
    for (var j = 0; j < barData.length; j++) {
        if (j < 10) {
            yData.push('0' + j + barData[j].name);
        } else {
            yData.push(j + barData[j].name);
        }
    }
    // ECharts配置
    var option = {
        // 标题
        title: {
            text: name_title,
            // subtext: subname,
            left: '300',
            top: 30,
            textStyle: {
                color: nameColor,
                fontFamily: name_fontFamily,
                fontSize: name_fontSize
            },
            // subtextStyle: {
            //     fontSize: subname_fontSize,
            //     fontFamily: name_fontFamily
            // }
        },
        // 提示框信息
        tooltip: {
            trigger: 'item',
            formatter: function(params) {
                var toolTiphtml = '';
                if (params.name === '南海诸岛') {
                    toolTiphtml = '';
                } else if (typeof(params.value)[2] === "undefined") {
                    toolTiphtml = params.value === 0 ? '' : (transferProvinceName(params.name) + ': ' + params.value);
                } else {
                    toolTiphtml = transferProvinceName(params.name) + ': ' + params.value[2];
                }
                return toolTiphtml;
            }
        },
        // 视觉映射，数据范围Low--High
        visualMap: {
            show: true,
            min: 0,
            max: 100000000,
            left: 'left',
            top: 'bottom',
            text: ['High', 'Low'], // 文本，默认为数值文本
            calculable: true,
            seriesIndex: [1],
            // 数据颜色范围，值越大颜色越深
            inRange: {
                color: ['#EFEFFF', '#4444FF']
            }
        },
        grid: {
            right: 25,
            top: 80,
            bottom: 20,
            width: '200'
        },
        xAxis: {
            show: false
        },
        yAxis: {
            type: 'category',
            inverse: true,
            nameGap: 16,
            axisLine: {
                show: false,
                lineStyle: {
                    color: '#ddd'
                }
            },
            axisTick: {
                show: false,
                lineStyle: {
                    color: '#ddd'
                }
            },
            // Y轴刻度标签，富文本展示
            axisLabel: {
                interval: 0,
                margin: 105,
                textStyle: {
                    align: 'left',
                    fontSize: 14,
                },
                rich: {
                    // 前三名, 序号颜色
                    a: {
                        color: '#fff',
                        backgroundColor: '#f0515e',
                        width: 20,
                        height: 20,
                        align: 'center',
                        borderRadius: 2
                    },
                    // 第四名之后, 序号颜色
                    b: {
                        color: '#fff',
                        backgroundColor: '#24a5cd',
                        width: 20,
                        height: 20,
                        align: 'center',
                        borderRadius: 2
                    },
                    // 前三名, 文字颜色
                    x: {
                        color: '#f0515e'
                    },
                    // 第四名之后, 文字颜色
                    y: {
                        color: '#24a5cd'
                    }
                },
                // 处理前三名, 及其他的标签
                formatter: function(params) {
                    if (parseInt(params.slice(0, 2)) < 3) {
                        return [
                            '{a|' + (parseInt(params.slice(0, 2)) + 1) + '}' + '  ' + '{x|' + transferProvinceName(params.slice(2)) + '}'
                        ].join('\n')
                    } else {
                        return [
                            '{b|' + (parseInt(params.slice(0, 2)) + 1) + '}' + '  ' + '{y|' + transferProvinceName(params.slice(2)) + '}'
                        ].join('\n')
                    }
                }
            },
            data: yData
        },
        // 地理坐标系组件,绘制地图,散点图等
        geo: {
            show: true,
            map: mapName,
            top: 100,
            left: 'left',
            right: '350',
            label: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: false,
                }
            },
            roam: true,
            itemStyle: {
                // 地图hover颜色
                emphasis: {
                areaColor: '#4444FF',
                }
            },
            //是否显示南海诸岛
            regions: [{
                name: "南海诸岛",
                value: 0,
                itemStyle: {
                    normal: {
                        areaColor: '#EFEFFF',
                        // opacity为0不绘制该图形
                        opacity: 1,
                        label: {
                            show: false
                        },
                    }
                }
            }],
        },
        series: [
            // 散点图, 蓝色的点
            {
                name: '散点图',
                type: 'scatter',
                coordinateSystem: 'geo',
                data: convertData(data),
                symbolSize: function(val) {
                    return val[2] / 10000000;
                },
                label: {
                    normal: {
                        formatter: function(obj) {
                            let name = transferProvinceName(obj.name);
                            return name || '';
                        },
                        position: 'right',
                        show: true
                    },
                    emphasis: {
                        show: true
                    }
                },
                itemStyle: {
                    normal: {
                        color: '#4444FF'
                    }
                }
            },
            // 地图
            {
                type: 'map',
                map: mapName,
                geoIndex: 0,
                aspectScale: 0.75, // 地图长宽比
                showLegendSymbol: false,
                label: {
                    normal: {
                        show: true
                    },
                    emphasis: {
                        show: false,
                        textStyle: {
                            color: '#fff'
                        }
                    }
                },
                roam: true,
                // itemStyle: {
                //   normal: {
                //     areaColor: '#031525',
                //     borderColor: '#3B5077',
                //   },
                //   emphasis: {
                //     areaColor: '#2B91B7'
                //   }
                // },
                animation: false,
                data: data
            },
            // 气泡散点图，红色气泡
            {
                name: '气泡散点图',
                type: 'scatter',
                coordinateSystem: 'geo',
                // 'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow', 'none'
                symbol: 'pin', //气泡
                symbolSize: function(val) {
                //     // 根据value值，设置symbol大小，根据实际情况自己调整
                    if (val[2] === 0) {
                        return 0;
                    }
                    var a = (maxSize4Pin - minSize4Pin) / (max - min);
                    var b = maxSize4Pin - a * max;
                return a* val[2]/40000 + b * 1.2;

                    // return 60
                },
                label: {
                    normal: {
                        show: true,
                        formatter: function(obj) {
                            return obj.data.value[2];
                        },
                        textStyle: {
                            color: '#fff',
                            fontSize: 9,
                        }
                    }
                },
                itemStyle: {
                    normal: {
                        // 气泡颜色
                        color: '#F62157',
                    }
                },
                zlevel: 6,
                data: convertData(data),
            },
            // 前五名，带有涟漪特效动画的散点（气泡）图，黄色
            {
                name: 'Top 5',
                type: 'effectScatter',
                coordinateSystem: 'geo',
                // data: convertData(data.sort(function(a, b) {
                //     return b.value - a.value;
                // }).slice(0, 5)),
                symbolSize: function(val) {
                    return val[2] / 120;
                },
                showEffectOn: 'render',
                rippleEffect: {
                    brushType: 'stroke'
                },
                hoverAnimation: true,
                label: {
                    normal: {
                        formatter: function(obj) {
                            let name = transferProvinceName(obj.name);
                            return name || '';
                        },
                        position: 'right',
                        show: false
                    }
                },
                itemStyle: {
                    normal: {
                        color: 'yellow',
                        shadowBlur: 10,
                        shadowColor: 'yellow'
                    }
                },
                zlevel: 1
            },
            // 柱状图
            {
                name: '柱状图',
                type: 'bar',
                roam: false,
                visualMap: false,
                barMaxWidth: 20,
                zlevel: 2,
                barGap: 0,
                itemStyle: {
                    normal: {
                        // 柱状图，渐变色
                        color: function(params) {
                            var colorList = [{
                                    colorStops: [{
                                        offset: 0,
                                        color: '#f0515e'
                                    }, {
                                        offset: 1,
                                        color: '#ef8554'
                                    }]
                                },
                                {
                                    colorStops: [{
                                        offset: 0,
                                        color: '#3c38e4'
                                    }, {
                                        offset: 1,
                                        color: '#24a5cd'
                                    }]
                                }
                            ];
                            if (params.dataIndex < 3) {
                                return colorList[0]
                            } else {
                                return colorList[1]
                            }
                        },
                    },
                    // 柱状图hover颜色
                    emphasis: {
                        color: "#f0515e"
                    },
                },
                label: {
                    normal: {
                        show: true,
                        position: 'right',
                        textBorderWidth: 0
                    }
                },
                data: barData
            }
        ]
    };
    myChart.setOption(option);

    // 将中文省份名称转换为拼音名称
    function transferProvinceName(name) {
        var provincesPinyin = ['Shanghai', 'Hebei', 'Shanxi', 'Neimenggu', 'Liaoning', 'Jilin', 'Heilongjiang', 'Jiangsu', 'Zhejiang', 'Anhui', 'Fujian', 'Jiangxi', 'Shandong', 'Henan', 'Hubei', 'Hunan', 'Guangdong', 'Guangxi', 'Hainan', 'Sichuan', 'Guizhou', 'Yunnan', 'Xizang', 'Shanxi1', 'Gansu', 'Qinghai', 'Ningxia', 'Xinjiang', 'Beijing', 'Tianjin', 'Chongqing', 'Xianggang', 'Aomen', 'Taiwan'];
        var provincesChinese = ['上海', '河北', '山西', '内蒙古', '辽宁', '吉林', '黑龙江', '江苏', '浙江', '安徽', '福建', '江西', '山东', '河南', '湖北', '湖南', '广东', '广西', '海南', '四川', '贵州', '云南', '西藏', '陕西', '甘肃', '青海', '宁夏', '新疆', '北京', '天津', '重庆', '香港', '澳门', '台湾'];
        var pinyinName = provincesPinyin[provincesChinese.indexOf(name)];
        return pinyinName || '';
    }

    // 用于处理散点图数据
    function convertData(data) {
        var res = [];
        for (var i = 0; i < data.length; i++) {
            var geoCoord = geoCoordMap[data[i].name];
            if (geoCoord) {
                res.push({
                    name: data[i].name,
                    value: geoCoord.concat(data[i].value),
                });
            }
        }
        return res;
    }
        myChart.setOption(option);
        //让图表跟随屏幕自动的去适应
        window.addEventListener("resize",function() {
            myChart.resize();
        });
    })();
